import React from 'react';
import Input from './components/input';
import Button from './components/button';
import './App.css';

function App() { // Load of components = mounting
  const [value, setValue] = React.useState('');
  const [products, setProducts] = React.useState([]);
  const [showTimer, setShowTimer] = React.useState(false);

  const [counter, setCounter] = React.useState(0);

  const callBackClick = (e) => {
    e.preventDefault();
    setProducts([...products, value]);
    setValue('');
  };

  React.useEffect(() => {
    if (products.length > 1) {
      setShowTimer(true)
    }
  }, [products]);

  React.useEffect(() => {
    if (showTimer) { // Before update = updating
      setTimeout(() => {
        setCounter(counter + 1)
      }, 1000);
    }
  }, [showTimer, counter]);

  return ( // render
    <div className="App">
      <Input title="Enter Text" value={value} setValue={setValue} sample="hello" />
      <Button type="text" label="Click me" callback={callBackClick} />

      {showTimer && <p>{`Display count: ${counter}`}</p>}
      <div className="flex justify-center">
        <ul style={{ listStyle: 'none'}}>
          {products?.map((product, key) => (
            <li key={key} className="m-2">{product}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
