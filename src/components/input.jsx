import React from 'react';

const Input = (props) => {
  return (
    <div>
      <label htmlFor="data" style={{ marginRight: 5 }}>{props.title}</label>
      <input type="text" name="data" placeholder="Enter text here" value={props.value} onChange={(e) => props.setValue(e.target.value)} />
    </div>
  );
};

export default Input;