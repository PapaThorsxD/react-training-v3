import React from 'react'

export default function button(props) {
  return (
    <button type={props.type} onClick={props.callback}>{props.label}</button>
  )
}
